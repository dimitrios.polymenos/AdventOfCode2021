#include <algorithm>
#include <iostream>
#include <numeric>
#include <unordered_map>

#include "common.h"

struct IntPairHasher
{
	size_t operator()(const std::pair<int, int>& p) const
	{
		return std::hash<int>{}(p.first) ^ std::hash<int>{}(p.second);
	}
};

int main()
{
	using segment_t = std::pair<std::pair<int, int>, std::pair<int, int>>;
	auto segments = parse_multiline_file("input.txt", [](std::ifstream& f) {
		int x1, y1, x2, y2 = 0;
		f >> x1;
		f.ignore(1);
		f >> y1;
		f.ignore(4);
		f >> x2;
		f.ignore(1);
		f >> y2;
		return segment_t{{x1, x2}, {y1, y2}};
	});

	std::unordered_map<std::pair<int, int>, int, IntPairHasher> coords;

	// partition the diagonals
	auto split = std::partition(segments.begin(), segments.end(), [](const segment_t& s) {
		return (s.first.first != s.first.second) && (s.second.first != s.second.second);
	});

	// problem 1
	{
		for (auto it = split; it != segments.end(); ++it)
		{
			const auto& s = *it;
			if (s.second.first == s.second.second)
			{
				auto start = std::min(s.first.first, s.first.second);
				auto end = std::max(s.first.first, s.first.second);
				for (int i = start; i < end; ++i)
					coords[{i, s.second.first}] += 1;
				coords[{end, s.second.first}] += 1;
			}
			else if (s.first.first == s.first.second)
			{
				auto start = std::min(s.second.first, s.second.second);
				auto end = std::max(s.second.first, s.second.second);
				for (int i = start; i < end; ++i)
					coords[{s.first.first, i}] += 1;
				coords[{s.first.first, end}] += 1;
			}
		}
		auto ac = std::accumulate(coords.cbegin(), coords.cend(), 0, [](int a, auto b) { return a + (b.second > 1); });
		std::cout << ac << "\n";
	}

	// problem 2
	{
		for (auto it = segments.begin(); it != split; ++it)
		{
			const auto& s = *it;
			auto x_step = s.first.first < s.first.second ? 1 : -1;
			auto y_step = s.second.first < s.second.second ? 1 : -1;
			auto y_accum = 0;
			for (int i = s.first.first; i != s.first.second; i += x_step)
			{
				coords[{i, s.second.first + y_accum}] += 1;
				y_accum += y_step;
			}
			coords[{s.first.second, s.second.second}] += 1;
		}
		auto ac = std::accumulate(coords.cbegin(), coords.cend(), 0, [](int a, auto b) { return a + (b.second > 1); });
		std::cout << ac << "\n";
	}

	return 0;
}
