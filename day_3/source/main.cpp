#include <algorithm>
#include <array>
#include <cstddef>
#include <iostream>
#include <string>
#include <tuple>

#include "common.h"

constexpr size_t sz = 12;  // from input

int main()
{
	auto lines = parse_multiline_file("input.txt", [](std::ifstream& s) {
		std::string l;
		std::getline(s, l);
		return l;
	});

	// problem 1
	{
		auto get_gamma = [](typename decltype(lines)::iterator begin, typename decltype(lines)::iterator end) {
			std::array<int, sz> scratch{};
			for (auto it = begin; it != end; ++it)
				for (size_t i = 0; i < sz; ++i)
					scratch[i] += (*it)[i] == '1' ? 1 : -1;
			std::string qwe(sz, '0');
			for (size_t i = 0; i < sz; ++i)
				qwe[i] = scratch[i] > 0 ? '1' : '0';
			return qwe;
		};

		const auto gamma_str = get_gamma(lines.begin(), lines.end());
		const auto gamma = std::strtoul(&gamma_str[0], nullptr, 2);
		const auto mask = std::numeric_limits<unsigned long>::max() >> ((sizeof(unsigned long) * 8) - gamma_str.size());
		const auto epsilon = (~gamma & mask);
		std::cout << gamma * epsilon << "\n";
	}

	// problem 2
	{
		auto iter_over = [](typename decltype(lines)::iterator begin,
		                    typename decltype(lines)::iterator end,
		                    bool prefer_ones,
		                    bool (*pred)(const ptrdiff_t&, const ptrdiff_t&)) {
			auto i = 0;
			do
			{
				// by problem definition (i < sz) && std::distance(begin, end) > 0
				if (std::distance(begin, end) == 1)
					return std::strtoul(&(*begin)[0], nullptr, 2);
				auto split = std::partition(begin, end, [i](const std::string& val) { return val[i] == '1'; });
				const auto dist_ones = std::distance(begin, split);
				const auto dist_zeores = std::distance(split, end);
				const auto cond = (dist_ones == dist_zeores) ? prefer_ones : pred(dist_ones, dist_zeores);
				std::tie(begin, end) = cond ? std::make_pair(begin, split) : std::make_pair(split, end);
				++i;
			} while (true);
		};

		const auto oxygen = iter_over(lines.begin(), lines.end(), true, [](const ptrdiff_t& a, const ptrdiff_t& b) {
			return std::greater<ptrdiff_t>{}(a, b);
		});
		const auto co2 = iter_over(lines.begin(), lines.end(), false, [](const ptrdiff_t& a, const ptrdiff_t& b) {
			return std::less<ptrdiff_t>{}(a, b);
		});

		std::cout << oxygen * co2 << "\n";

		return 0;
	}
}
