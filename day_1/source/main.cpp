#include <iostream>
#include <cstdint>
#include <cassert>
#include <numeric>

#include "common.h"

int main()
{
	auto lines = parse_multiline_file<size_t>("input.txt");

	//problem 1
	{
		assert(lines.size() > 1);
		size_t larger_than_prev = 0;
		for (size_t i = 1; i < lines.size(); ++i)
			larger_than_prev += lines[i] > lines[i - 1];
		std::cout << larger_than_prev << "\n";
	}

	// problem 2
	{
		constexpr size_t sliding_window_size = 3;
		static_assert(sliding_window_size > 1, "");
		assert(lines.size() > sliding_window_size + 1);
		size_t larger_than_prev = 0;
		auto prev_window_sum = std::accumulate(lines.cbegin(), lines.cbegin() + sliding_window_size, static_cast<size_t>(0));
		for (auto it = lines.cbegin() + 1; it != lines.cend() - sliding_window_size + 1; ++it)
		{
			auto window_sum = std::accumulate(it, it + sliding_window_size, static_cast<size_t>(0));
			larger_than_prev += window_sum > prev_window_sum;
			prev_window_sum = window_sum;
		}

		std::cout << larger_than_prev << "\n";
	}

	return 0;
}
