#include <iostream>
#include <cstdint>
#include <cassert>
#include <numeric>
#include <string>
#include <cassert>

#include "common.h"

int main()
{
	auto lines = parse_multiline_file("input.txt", [](std::ifstream& s) {
		std::string l;
		std::getline(s, l);
		size_t advance = 0;
		switch (l[0])
		{
		case 'f':
			advance = std::size("forward");
			break;
		case 'u':
			advance = std::size("up");
			break;
		case 'd':
			advance = std::size("down");
			break;
		default:
			assert(false);
		}
		return std::make_pair(l[0], std::atoi(l.c_str() + advance));
	});

	//problem 1
	{
		int horizonal_pos = 0;
		int depth = 0;
		for (const auto& l : lines)
		{
			switch (l.first)
			{
			case 'f':
				horizonal_pos += l.second;
				break;
			case 'd':
				depth += l.second;
				break;
			case 'u':
				depth -= l.second;
				break;
			}
		}
		std::cout << horizonal_pos * depth << "\n";
	}

	// problem 2
	{
		int horizonal_pos = 0;
		int depth = 0;
		int aim = 0;
		for (const auto& l : lines)
		{
			switch (l.first)
			{
			case 'f':
				horizonal_pos += l.second;
				depth += aim * l.second;
				break;
			case 'd':
				aim += l.second;
				break;
			case 'u':
				aim -= l.second;
				break;
			}
		}
		std::cout << horizonal_pos * depth << "\n";

	}

	return 0;
}
