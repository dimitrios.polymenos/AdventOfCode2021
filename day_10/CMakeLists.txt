cmake_minimum_required (VERSION 3.4)

project(day10 LANGUAGES CXX)

file(GLOB CPP_FILES source/*.cpp)
file(GLOB H_FILES include/*.h)

include_directories(${common_INCLUDE_DIRS})

add_executable(${PROJECT_NAME} ${CPP_FILES} ${H_FILES})

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
	target_compile_options(day10 PRIVATE /W4)
	target_compile_options(day10 PRIVATE -EHsc)
	set_target_properties(${PROJECT_NAME} PROPERTIES LINK_FLAGS "/SUBSYSTEM:CONSOLE")
endif()

target_link_libraries(${PROJECT_NAME} common)
