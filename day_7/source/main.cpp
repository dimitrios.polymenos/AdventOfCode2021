#include <algorithm>
#include <iostream>
#include <limits>
#include <numeric>

#include "common.h"

int main()
{
	ParserHelper p("input.txt");
	auto crabs = p.read_delimited<int>(',');
	std::sort(crabs.begin(), crabs.end());

	auto calc_min_distance = [&crabs](auto reduce_op) {
		const auto min_vert = crabs[0];
		const auto max_vert = crabs.back();
		int min_dist = std::numeric_limits<int>::max();
		for (int i = min_vert; i < max_vert; ++i)
		{
			auto ith_distance = std::accumulate(
			    crabs.cbegin(), crabs.cend(), 0, [i, &reduce_op](int init, int elem) { return init += reduce_op(elem, i); });
			min_dist = std::min(ith_distance, min_dist);
		}
		return min_dist;
	};

	// problem 1
	{
		std::cout << calc_min_distance([](int elem, int i) { return std::abs(elem - i); }) << "\n";
	}
	// problem 2
	{
		auto sum_of_ints = [](int term) { return term * (term + 1) / 2; };
		std::cout << calc_min_distance([sum_of_ints](int elem, int i) { return sum_of_ints(std::abs(elem - i)); }) << "\n";

	}

	return 0;
}
